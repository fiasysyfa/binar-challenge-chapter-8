const Navbar = () => {
    return(
        <div>
            <nav class="navbar navbar-expand-lg navbar-dark bg-primary py-3">
    <div class="container">
        <a class="navbar-brand" href="/">React Website</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/">Create Player</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/edit-player">Edit Player</a>
                </li>
                <li class="nav-item">
                <a class="nav-link" aria-current="page" href="/search-player">Search Player</a>
                </li>
            </ul>
        </div>
    </div>
</nav>
        </div>
    );
}

export default Navbar;
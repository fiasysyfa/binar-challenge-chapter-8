const CreatePlayer = () => {
    return(
<body>
    <div class="container my-5">
        <div class="card p-5">
            <center>
                <h1>Create Player</h1>
            </center>
            <form action="/api/v1/articles" method="POST">
                <div class="my-3">
                    <label for="username" class="form-label h5">Username</label>
                    <input type="text" class="form-control" id="username" name="username" aria-describedby="username"/>
                </div>
                <div class="mb-3">
                    <label for="email" class="form-label h5">Email</label>
                    <input type="email" class="form-control" id="email" name="email" aria-describedby="email"/>
                </div>
                <div class="mb-3">
                    <label for="password" class="form-label h5">Password</label>
                    <input type="password" class="form-control" id="password" name="password" aria-describedby="password"/>
                </div>
                <button class="btn btn-primary" type="submit">Post</button>
            </form>
        </div>
    </div>
</body>

    );
}

export default CreatePlayer;
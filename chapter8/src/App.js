import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import Navbar from './Navbar';

import CreatePlayer from './CreatePlayer';
import EditPlayer from './EditPlayer';
import SearchPlayer from './SearchPlayer';

function App() {
  return (
    <Router>
      <div className="App">
        <Navbar/>
      <Switch>
          <Route path="/edit">
            <EditPlayer />
          </Route>
          <Route path="/search">
            <SearchPlayer />
          </Route>
          <Route path="/">
            <CreatePlayer />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
